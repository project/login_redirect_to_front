Login Redirect to Front
https://www.drupal.org/project/login_redirect_to_front
======================================================

INTRODUCTION
-----------
Redirect to front page after login

REQUIREMENTS
------------

This module has no requires

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
-------------

Module has no configuration

MAINTAINERS
-----------

Current maintainers:
* Andrey Ivnitsky - https://www.drupal.org/u/itcrowd72
